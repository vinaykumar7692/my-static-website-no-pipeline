import subprocess

def calculate_change_percentage():
    # Get the diff between the source and target branches
    diff_output = subprocess.check_output(['git', 'diff', f'$CI_MERGE_REQUEST_TARGET_BRANCH_NAME..$CI_COMMIT_REF_NAME', 'my-static-website-no-pipeline/README.md'])
    
    # Calculate the total number of lines changed
    total_lines_changed = len(diff_output.splitlines())
    
    # Calculate the total number of lines in the file
    total_lines_in_file = len(open('path/to/file').readlines())
    
    # Calculate the percentage of changes
    change_percentage = (total_lines_changed / total_lines_in_file) * 100

    return change_percentage

if __name__ == "__main__":
    percentage = calculate_change_percentage()
    print(f"Percentage of changes: {percentage}%")

    # Check if changes exceed 5%
    if percentage > 5:
        print("Changes exceed 5%. Blocking merge request.")
        exit(1)
    else:
        print("Changes within 5%. Allowing merge request.")
